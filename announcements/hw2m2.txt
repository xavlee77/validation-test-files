Hey all,

As part of HW2M2, there will be validation test cases run when you submit on Gradescope. These are automatically run and should run around 5 minutes after you submit. You can submit as many times as you like. These tests are not complete and do not guarantee that your submission is correct in every aspect. These are for making sure your submission will be compatible with the autograder.

<strong>It is imperative that you pass all validation test cases. Failure to do so means that there is a problem with your submission and the autograder will heavily penalize you.</strong>

Below is a breakdown of each test case and what it does for the validation tests. For this assignment, the validation tests will run your crawler on the contents of the following site (you may use this for local testing): <a href="https://mike2151.github.io/555_m2_server_example/" target="_blank" rel="noopener noreferrer">https://mike2151.github.io/555_m2_server_example/</a>. The contents of which are located here: <a href="https://bitbucket.org/upenn-cis555/validation-test-files/src/master/hw2m2/" target="_blank" rel="noopener noreferrer">https://bitbucket.org/upenn-cis555/validation-test-files/src/master/hw2m2/</a>. We set up one channel using the following URL:

<pre>
http://localhost:45555/create/HTML?xpath=/html/body/h1</pre>

Therefore, the html file in the GitHub repository should match this xpath.

Validation test cases:

<strong>test_compiles_correctly</strong>- makes sure your code compiles. If this test fails, then all others will also fail

<strong>test_webcrawler_ran</strong>- tests that the crawler is able to crawl the html file above. If this test fails, then all others except test_compiles_correctly will also fail

<strong>test_webserver_runs</strong>- ensures that your webserver is able to run and does not encounter compilation errors

<strong>test_can_register_and_login</strong>- ensures that a user can be registered using username and password and can login with the same username and password. This is the same as the HW2M1 validation test

<strong>test_created_channel_displays_on_home_page</strong>- ensures the created channel using the url above shows on the home page once logged in

<strong>test_can_login_after_running_crawler</strong>- ensures that the user can login after running the crawler

<strong>test_document_contents_are_correct</strong>- ensures that the channel displays the content of the HTML file above which should be captured by the xpath after a crawler run

Please make sure you pass all test cases once you submit. If you have any questions, feel free to followup below.

There will also be a Gradescope output block that contains the output of your submission for the following points:

1. Output of the compiler
2. Output of the server during registration, logging in, and channel creation
3. Output of your crawler on the files provided
4. Output of your server after logging in after the crawler run
5. Output of checking the channel

#pin