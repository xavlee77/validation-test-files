Hey all,

As part of HW1M1, there will be validation test cases run when you submit on Gradescope. These are automatically run and should run around 5 minutes after you submit. You can submit as many times as you like. These tests are not complete and do not guarantee that your submission is correct in every aspect. These are for making sure your submission will be compatible with the autograder.

<strong>It is imperative that you pass all validation test cases. Failure to do so means that there is a problem with your submission and the autograder will heavily penalize you.</strong>

Below is a breakdown of each test case and what it does for the validation tests. For this set of validation tests, we ask your server to serve up a static file and ensure that the content length and the contents of the served up file match the original file. The file that is served can be found here:  <a href="https://bitbucket.org/upenn-cis555/validation-test-files/src/master/hw1m1/test.html" target="_blank" rel="noopener noreferrer">https://bitbucket.org/upenn-cis555/validation-test-files/src/master/hw1m1/test.html</a>

Validation test cases:

<ul>
<li><strong>test_document_contents_properly_delivered </strong>- tests whether the contents of the served file exactly match the contents of the original file</li>
<li><strong>test_submission_compiles</strong>- Tests to see if submission compiles (looks for "BUILD SUCCESS" in compilation output)</li>
<li><strong>test_content_length_header_properly_computed_and_included</strong>- tests that the content-length header is present and is accurate</li>
<li><strong>test_required_files_submitted</strong>- makes sure the right files were submitted</li>
<li><strong>test_uses_threads_and_not_disallowed_code</strong>- ensures that you use threads and not disallowed imports like BlockingQueue</li>
</ul>

Please make sure you pass all test cases once you submit. If you have any questions, feel free to followup below.

There will also be a Gradescope output block that contains the output of your submission for the following points:

1. Output of the file that your server served
2. Output of the headers that went along with the served file
3. Output of the server during validation test run
4. Output of server compilation

#pin
<div></div>
<div></div>
<div></div>